package com.example.firstproject.model.data

class RepositoryMovie(private val dao: MovieDao) {


   val movies = dao.getAllMovie()

    suspend fun insert(dataMovie: DataMovie): Long {
        return dao.insertMovie(dataMovie)
    }

    suspend fun update(dataMovie: DataMovie): Int {
        return dao.updateMovie(dataMovie)
    }

    suspend fun delete(dataMovie: DataMovie): Int {
        return dao.deleteMovie(dataMovie)
    }

    suspend fun deleteAll(): Int {
        return dao.deleteAll()
    }


}