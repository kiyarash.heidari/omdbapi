package com.example.firstproject.model.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [DataMovie::class], version = 4,exportSchema = false)
abstract class DatabaseMovie : RoomDatabase() {

    abstract val movieDao: MovieDao

    companion object {

        private val INSTANCE: DatabaseMovie? = null

        fun getInstance(context: Context): DatabaseMovie {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        DatabaseMovie::class.java,
                        "movie_data_database"
                    ).fallbackToDestructiveMigration().build()
                }
                return instance
            }
        }
    }
}