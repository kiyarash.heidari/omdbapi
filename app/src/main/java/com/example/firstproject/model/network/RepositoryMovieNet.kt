package com.example.firstproject.model.network

import android.util.Log
import com.example.firstproject.model.Search
import com.wil8dev.pixture.model.ModelMovie
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepositoryMovieNet(){

    private var listOfModelMovie: MutableList<ModelMovie> = mutableListOf()
    private var retrofit = RetrofitClient.instance

    var arrayList = ArrayList<ModelMovie>()

    fun retrofitSearch(searchText: String): ArrayList<ModelMovie> {

        val resultSearch = retrofit.create(IMyAPI::class.java).search(searchText)
        resultSearch.enqueue(object : Callback<Search> {
            override fun onFailure(call: Call<Search>, t: Throwable) {
                Log.e("Search", "Erro : ${t}")
            }
            override fun onResponse(call: Call<Search>, response: Response<Search>) {
                resetList()
                val allSearch = response.body()?.resultSearches
                allSearch?.let {
                    for (movie in allSearch) {
                        if (movie.title != null && movie.poster != null) {
//                            listOfModelMovie.add(movie)
                            arrayList.add(movie)
                        Log.i("Response", "${movie.country}")
                        }
                        Log.i("Response", "$movie")
                    }
                }
            }
        })
        return arrayList
    }
    fun resetList() {
        listOfModelMovie.clear()
    }

}