package com.example.firstproject.model

import com.google.gson.annotations.SerializedName
import com.wil8dev.pixture.model.ModelMovie

data class Search(
    @SerializedName("Search")
    val resultSearches: ArrayList<ModelMovie>?
)