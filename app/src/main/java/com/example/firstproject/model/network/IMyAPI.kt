package com.example.firstproject.model.network

import com.example.firstproject.model.Search
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

const val OMBD_API_KEY: String = "229e5323"

interface IMyAPI {

    @GET("/")
    fun search(@Query("s") searchText: String, @Query("apiKey") ombd_api_key: String = OMBD_API_KEY): Call<Search>

}