package com.example.firstproject.model.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MovieDao {

    @Insert
    suspend fun insertMovie(dataMovie: DataMovie): Long

    @Update
    suspend fun updateMovie(dataMovie: DataMovie): Int

    @Delete
    suspend fun deleteMovie(dataMovie: DataMovie): Int

    @Query("DELETE FROM Info_Movie_local ")
    suspend fun deleteAll(): Int

    @Query("SELECT * FROM Info_Movie_local")
    fun getAllMovie(): LiveData<List<DataMovie>>




}