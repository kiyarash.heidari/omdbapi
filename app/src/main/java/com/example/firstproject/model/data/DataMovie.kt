package com.example.firstproject.model.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Info_Movie_local")
data class DataMovie(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "movie_id")
    var id: Int,

    @ColumnInfo(name = "movie_title")
    var title: String,

    @ColumnInfo(name = "movie_poster")
    var poster: String,

    @ColumnInfo(name = "movie_year_making")
    var year: String


)