package com.example.firstproject.viewmodel

import android.app.Application
import androidx.databinding.Observable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.firstproject.model.data.DataMovie
import com.example.firstproject.model.data.DatabaseMovie
import com.example.firstproject.model.data.RepositoryMovie
import com.example.firstproject.model.network.RepositoryMovieNet
import com.wil8dev.pixture.model.ModelMovie
import kotlinx.coroutines.launch
import kotlin.collections.ArrayList


class ViewModelFSearch(application: Application) : AndroidViewModel(application),Observable {

    private val context = getApplication<Application>().applicationContext

    var arrayListMutableLiveData = MutableLiveData<ArrayList<ModelMovie>>()
    var arrayList = ArrayList<ModelMovie>()
    val dao = DatabaseMovie.getInstance(context).movieDao
    val repositoryMovie = RepositoryMovie(dao)

    fun getArrayList(): MutableLiveData<ArrayList<ModelMovie>> {

        arrayListMutableLiveData.value = arrayList
        return arrayListMutableLiveData
    }

    fun search(searchText: String) {
        val repository = RepositoryMovieNet()
        arrayList = repository.retrofitSearch(searchText)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    fun initUpdateAndDelete(dataMovie: DataMovie) {
        viewModelScope.launch { repositoryMovie.insert(dataMovie) }
    }
}
