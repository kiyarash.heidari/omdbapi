package com.example.firstproject.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.firstproject.model.data.RepositoryMovie
import java.lang.IllegalArgumentException

class ViewModelFactoryMovie(private val repositoryMovie: RepositoryMovie) :
    ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ViewModelDBMovie::class.java)) {

            return ViewModelDBMovie(repositoryMovie) as T
        }
        throw IllegalArgumentException("Unknown View Model Class")
    }
}