package com.example.firstproject.viewmodel

import androidx.databinding.Observable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.firstproject.model.data.DataMovie
import com.example.firstproject.model.data.RepositoryMovie
import com.example.firstproject.model.network.RepositoryMovieNet
import com.wil8dev.pixture.model.ModelMovie
import kotlinx.coroutines.launch


class ViewModelDBMovie(
    private val repositoryMovie: RepositoryMovie
) : ViewModel(), Observable {

    val dataMovie = repositoryMovie.movies
    var arrayList = ArrayList<ModelMovie>()

    fun search(searchText: String) {
        val repository = RepositoryMovieNet()
        arrayList = repository.retrofitSearch(searchText)
    }

    fun initUpdateAndDelete(dataMovie: DataMovie) {
        viewModelScope.launch { repositoryMovie.delete(dataMovie) }
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }
}
