package com.example.firstproject.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firstproject.R
import com.example.firstproject.view.adapters.AdapterRecyFFavorite
import com.example.firstproject.model.data.DataMovie
import com.example.firstproject.model.data.DatabaseMovie
import com.example.firstproject.model.data.RepositoryMovie
import com.example.firstproject.databinding.ThirdFragmentBinding
import com.example.firstproject.viewmodel.ViewModelDBMovie
import com.example.firstproject.viewmodel.ViewModelFactoryMovie

class FragmentFavorite : Fragment() {

    private lateinit var binding: ThirdFragmentBinding
    private lateinit var viewModelDBMovie: ViewModelDBMovie
    private lateinit var adapterRecyMovie: AdapterRecyFFavorite

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.third_fragment, container, false)

        val dao = DatabaseMovie.getInstance(requireContext()).movieDao
        val repositoryMovie =RepositoryMovie(dao)

        val factory = ViewModelFactoryMovie(repositoryMovie)

        viewModelDBMovie = ViewModelProvider(this, factory).get(ViewModelDBMovie::class.java)
        binding.myModelView = viewModelDBMovie
        binding.lifecycleOwner = this

        viewModelDBMovie.dataMovie.observe(this, Observer {viewModelDBMovie->
            adapterRecyMovie = AdapterRecyFFavorite(viewModelDBMovie,this::listItemClicked)
            binding.recy.layoutManager = LinearLayoutManager(requireContext())
            binding.recy.adapter = adapterRecyMovie
            adapterRecyMovie.notifyDataSetChanged()
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }


    private fun listItemClicked(dataMovie: DataMovie) {

        Toast.makeText(context, "Deleted  ${dataMovie.title}", Toast.LENGTH_SHORT).show()
        viewModelDBMovie.initUpdateAndDelete(dataMovie)
    }
}
