package com.example.firstproject.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.firstproject.R
import kotlinx.android.synthetic.main.fragment_first.view.*

class FragmentFirst : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.btn_start.setOnClickListener {

            val message: String = view.edt_nameMovie.text.toString()
            val frag_secound = FragmentSearch()

            val bundle = Bundle()
            bundle.putString("key", message)
            Log.i("LOOOG", "1:  $message")

            frag_secound.arguments = bundle
            val manager = fragmentManager
            val frag_transaction = manager?.beginTransaction()
            frag_transaction?.replace(R.id.fragmentContainer,frag_secound)
            frag_transaction?.commit()
        }
    }
}