package com.example.firstproject.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.firstproject.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item->
        when(item.itemId){
            R.id.navigation_home -> {
                println("home pressed")
                replaceFragment(FragmentFirst())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                println("map pressed")
                replaceFragment(FragmentSearch())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_fav -> {
                println("cart pressed")
                replaceFragment(FragmentFavorite())
                return@OnNavigationItemSelectedListener true
            }
        }

        false

    }

    private fun replaceFragment(fragment: Fragment){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        replaceFragment(FragmentFirst())

    }



}