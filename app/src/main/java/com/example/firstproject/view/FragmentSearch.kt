package com.example.firstproject.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firstproject.R
import com.example.firstproject.view.adapters.AdapterRecyFSearch
import com.example.firstproject.model.data.DataMovie
import com.example.firstproject.databinding.FragmentSearchBinding
import com.example.firstproject.viewmodel.ViewModelFSearch


class FragmentSearch : Fragment() {

    private lateinit var binding: FragmentSearchBinding
    private lateinit var viewModelFSearch: ViewModelFSearch
    private lateinit var adapterRecyFSearch: AdapterRecyFSearch

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)

        viewModelFSearch = ViewModelProviders.of(this).get(ViewModelFSearch::class.java)
        binding.viewmodel = viewModelFSearch
        binding.lifecycleOwner = this

        if (arguments?.getString("key") != null) {
            val str = arguments!!.getString("key")
            viewModelFSearch.search(str.toString())
        }

        viewModelFSearch.getArrayList()
            .observe(this, androidx.lifecycle.Observer { viewModelFSearch ->

                adapterRecyFSearch = AdapterRecyFSearch( this::listItemClicked,viewModelFSearch)
                binding.recyFragmentSecond.layoutManager = LinearLayoutManager(requireContext())
                binding.recyFragmentSecond.adapter = adapterRecyFSearch
            })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun listItemClicked(dataMovie: DataMovie) {
        Toast.makeText(context, "save to favorite  ${dataMovie.title}", Toast.LENGTH_SHORT).show()
        viewModelFSearch.initUpdateAndDelete(dataMovie)
    }
}

