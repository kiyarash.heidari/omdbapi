package com.example.firstproject.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.firstproject.R
import com.example.firstproject.model.data.DataMovie
import com.example.firstproject.databinding.ListItemFavoriteBinding
import kotlinx.android.synthetic.main.list_item_favorite.view.*


class AdapterRecyFFavorite(
    private val dataMovieList :List<DataMovie>,
    private val clickListener: (DataMovie) -> Unit
) :
    RecyclerView.Adapter<AdapterRecyFFavorite.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)

        val binding: ListItemFavoriteBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.list_item_favorite, parent, false)

        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return dataMovieList.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(dataMovieList[position],clickListener)
    }


    class MyViewHolder(val binding: ListItemFavoriteBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(dataMovie: DataMovie, clickListener: (DataMovie) -> Unit) {
            Log.i("Tag", "111111 ${dataMovie.title} ")

            binding.txtTitle1.text = dataMovie.title

            Glide.with(binding.root)
                .load(dataMovie.poster)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(itemView.img_poster1)

            binding.btnDelete.setOnClickListener {
                clickListener(dataMovie)
            }
        }
    }
}
