package com.example.firstproject.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.firstproject.R
import com.example.firstproject.model.data.DataMovie
import com.example.firstproject.databinding.ListItemSearchBinding
import com.wil8dev.pixture.model.ModelMovie
import kotlinx.android.synthetic.main.list_item_search.view.*


class AdapterRecyFSearch(
    private val clickListener: (DataMovie) -> Unit,
    private val dataMovieList: ArrayList<ModelMovie>
) :
    RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ListItemSearchBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.list_item_search, parent, false)

        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return dataMovieList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(dataMovieList[position], clickListener)
    }
}

class MyViewHolder(private val binding: ListItemSearchBinding) :
    RecyclerView.ViewHolder(binding.root) {


    fun bind(modelMovie: ModelMovie, clickListener: (DataMovie) -> Unit) {

        binding.txtTitle.text = modelMovie.title
        binding.txtYear.text = modelMovie.year
        binding.txtCountry.text = modelMovie.country

        Glide.with(binding.root)
            .load(modelMovie.poster)
            .centerCrop()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(itemView.img_poster)

        binding.favorite.setOnClickListener {

            val dataMovie = DataMovie(0, modelMovie.title, modelMovie.poster, modelMovie.year)

            clickListener(dataMovie)
        }
    }
}

